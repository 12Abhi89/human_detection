# pedestrian Detection
import cv2
import time
import smtplib
import numpy as np

from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from imutils.object_detection import non_max_suppression


class HumanDetection:
    def __init__(self):
        pass

    def __send_mail(self, captured_image):
        mail_content = "Movement detected"
        # The mail addresses and password
        sender_address = 'autotesting37@gmail.com'  # FROM
        sender_pass = 'Niwm3TVCViuVpxu'
        receiver_address = 'abhi2d3y@gmail.com'  # To
        # Setup the MIME
        message = MIMEMultipart()
        message['From'] = sender_address
        message['To'] = receiver_address
        message['Subject'] = "Security Alert"  # The subject line
        # The body and the attachments for the mail
        message.attach(MIMEText(mail_content, 'plain'))
        message.attach(MIMEImage(captured_image))
        # Create SMTP session for sending the mail
        session = smtplib.SMTP('smtp.gmail.com', 587)  # use gmail with port
        session.starttls()  # enable security
        session.login(sender_address, sender_pass)  # login with mail_id and password
        text = message.as_string()
        session.sendmail(sender_address, receiver_address, text)
        session.quit()
        return

    def human_detection(self):
        cam = cv2.VideoCapture(0) # from webcam stream for windows
        hog = cv2.HOGDescriptor()
        hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        while True:
            ret, image = cam.read()  # video to image conversion
            original = image.copy()
            (rects, weights) = hog.detectMultiScale(image, winStride=(4, 4), padding=(8, 8), scale=1.0)
            for (x, y, w, h) in rects:
                cv2.rectangle(original, (x, y), (x + w, y + h), (255, 255, 0), 2)
            rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])  # if we dont give rectangle collide
            pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

            for (xa, ya, wa, ha) in pick:
                cv2.rectangle(image, (xa, ya), (xa + w, ya + h), (255, 255, 0), 2)
                cv2.imwrite("image.jpg", image)
                pic = open("image.jpg", "rb")
                self.__send_mail(pic.read())
                pic.close()
                time.sleep(1)
            # cv2.imshow("before_nms", original)
            # cv2.imshow("after_nms", image)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cam.release()
        cv2.destroyAllWindows()

